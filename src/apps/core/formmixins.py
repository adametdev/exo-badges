from braces.forms import UserKwargModelFormMixin
# we use django-braces to have current user
# http://django-braces.readthedocs.io/en/latest

class UserFormMixin(UserKwargModelFormMixin):
    """
    Add current user into current object to save.
    """
    def save(self, commit=True):
        """
        Override save to manually save with current user when a object
        is created.
        """
        obj = super(UserFormMixin, self).save(commit=False)

        if commit and self.user:
            obj.user = self.user
            obj.save()

        return obj
