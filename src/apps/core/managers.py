from django.db import models


class BaseManager(models.Manager):
    """
    Specific manager for base models.
    """
    def get_queryset(self):
        """
        Override default queryset to filter and order base models.
        """
        return super(BaseManager, self).get_queryset().filter(deleted=0).order_by('-id')
