from django.db import models

from .managers import BaseManager


class TimeStampModel(models.Model):
    """
    An abstract base class model that provides self-updating and
    created fields.
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TitleDescriptionModel(models.Model):
    """
    An abstract base class model that provides title and description
    fields.
    """
    title = models.CharField(max_length=150, blank=True)
    description = models.TextField(blank=True)

    class Meta:
        abstract = True


class BaseModel(TimeStampModel):
    """
    A base model for all apps models.
    """
    deleted = models.PositiveSmallIntegerField(default=0)

    class Meta:
        abstract = True

    def delete(self, using=None, keep_parents=False):
        """
        Override delete methode to simulate object deletion with
        deleted set to 1.
        """
        self.deleted = 1
        self.save()

    objects = BaseManager()
