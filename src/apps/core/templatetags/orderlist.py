from django import template
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe


register = template.Library()

@register.simple_tag
def orderlist(url, field):
    """
    Generates ordering arrows to order datas in list mode.
    """
    url_asc = reverse(url, args=[field, 'asc'])
    url_desc = reverse(url, args=[field, 'desc'])

    orderlist = """<a href=\"{0}\">
                    <i class=\"fa fa-long-arrow-down\"></i>
                </a>
                <a href=\"{1}\">
                    <i class=\"fa fa-long-arrow-up\"></i>
                </a>""".format(url_asc, url_desc)

    return mark_safe(orderlist)
