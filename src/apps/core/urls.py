from django.conf.urls import url

from .views import HomePageView


urlpatterns = [
    url(
        regex = r'^$',
        view = HomePageView.as_view(),
        name = 'home'
    ),
    url(
        regex = r'^home$',
        view = HomePageView.as_view(),
        name = 'home'
    ),
]
