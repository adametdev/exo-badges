class ModelNameViewMixin(object):
    """
    Add model names to all views.
    """
    def get_context_data(self, **kwargs):
        """
        Overrides to add model names in templates.
        """
        kwargs = super(ModelNameViewMixin, self).get_context_data()
        kwargs['model_name'] = self.model._meta.verbose_name
        kwargs['model_name_plural'] = self.model._meta.verbose_name_plural
        return kwargs

class ListViewMixin(object):
    """
    Add specific behaviors to list views.
    """
    def get_queryset(self):
        """
        Add filters asked by user with list fields to order results.
        """
        queryset = super(ListViewMixin, self).get_queryset()

        if 'field' in self.kwargs and 'order_type' in self.kwargs:
            order_by = self.kwargs['field']
            if self.kwargs['order_type'] == 'desc':
                order_by = '-' + order_by
            queryset = queryset.order_by(order_by)

        return queryset
