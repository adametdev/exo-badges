from django.conf import settings
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView

from braces.views import LoginRequiredMixin


class HomePageView(LoginRequiredMixin, TemplateView):
    """
    A template view to implement Home page.
    """
    template_name = 'index.html'
