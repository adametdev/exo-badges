from django.contrib import admin

from .models import Model3d, Badge


@admin.register(Model3d, Badge)
class ResourcesAdmin(admin.ModelAdmin):
    pass
