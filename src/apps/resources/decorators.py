from django.db.models.signals import post_save

from .signals import model3d_views_callback


def handle_model_views(model):
    """
    Handler to connect post_save signal to a specific model.
    """
    post_save.connect(model3d_views_callback, sender=model)
    return model
