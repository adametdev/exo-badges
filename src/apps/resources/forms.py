from django import forms

# from apps.core.widgets import select2

from apps.core.formmixins import UserFormMixin

from .models import Model3d, Badge


class Model3dForm(UserFormMixin, forms.ModelForm):
    """
    A class to implement form for 3d models.
    We use UserKwargModelFormMixin from django-braces to have current user in the instance.
    """
    title = forms.CharField(
        label = 'Titre',
        max_length = 254,
        min_length = 2,
        widget = forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Entrez le titre'
        }),
    )
    description = forms.CharField(
        label = 'Description',
        max_length = 1000,
        widget = forms.Textarea(attrs={
            'class': 'form-control',
            'placeholder': 'Entrez la description'
        }),
        required = False,
    )
    image = forms.ImageField(
        label = 'Description',
        widget = forms.ClearableFileInput(attrs={
            'class': 'form-control',
            'placeholder': 'Téléchargez votre image'
        }),
        required = False,
    )

    class Meta:
        model = Model3d
        fields = (
            'title', 'description', 'image'
        )


class BadgeForm(forms.ModelForm):
    """
    A class to implement form for badges.
    """
    title = forms.CharField(
        label = 'Titre',
        max_length = 254,
        min_length = 2,
        widget = forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Entrez le titre'
        }),
    )
    description = forms.CharField(
        label = 'Description',
        max_length = 1000,
        widget = forms.Textarea(attrs={
            'class': 'form-control',
            'placeholder': 'Entrez la description'
        }),
        required = False,
    )

    class Meta:
        model = Badge
        fields = (
            'title', 'description'
        )
