from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

from apps.core.models import BaseModel, TitleDescriptionModel

from .decorators import handle_model_views


@handle_model_views
class Model3d(TitleDescriptionModel, BaseModel):
    """
    A class to implement 3d models.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    views = models.IntegerField(default=0)
    image = models.ImageField(blank=True)

    def get_absolute_url(self):
        return reverse('resources:model3d_detail', kwargs={'pk': self.pk})

    def __str__(self):
        """
        Return 3d models's title
        """
        return self.title


class Badge(TitleDescriptionModel, BaseModel):
    """
    A class to implement badges.
    """
    def get_absolute_url(self):
        return reverse('resources:badge_detail', kwargs={'pk': self.pk})

    def __str__(self):
        """
        Return badges's title
        """
        return self.title
