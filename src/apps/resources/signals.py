from django.shortcuts import get_object_or_404


def model3d_views_callback(sender, **kwargs):
    """
    Receiver of post_save signal to handle model 3d views.
    """
    from .models import Badge

    # When a model 3d reaches 1000 views, user related to it get the Star Badge
    model3d = kwargs['instance']
    if model3d.views == 1000:
        star_badge = get_object_or_404(Badge, title='Star')
        model3d.user.badges.add(star_badge)
        model3d.user.save()
