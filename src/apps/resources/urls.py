from django.conf.urls import url

from .views import *


# Model3d urls
urlpatterns = [
    url(
        r'^resources/model3d/(?P<pk>\d+)/$',
        Model3dDetailView.as_view(),
        name='model3d_detail'
    ),
    url(
        r'^resources/model3d/$',
        Model3dListView.as_view(),
        name='model3d_list'
    ),
    url(
        r'^resources/model3d/order/(?P<field>\w+)/type/(?P<order_type>\w+)/$',
        Model3dListView.as_view(),
        name='model3d_list_ordered'
    ),
    url(
        r'^resources/model3d/new/$',
        Model3dCreateView.as_view(),
        name='model3d_creation'
    ),
]


# Badge urls
urlpatterns += [
    url(
        r'^resources/badge/(?P<pk>\d+)/$',
        BadgeDetailView.as_view(),
        name='badge_detail'
    ),
    url(
        r'^resources/badge/$',
        BadgeListView.as_view(),
        name='badge_list'
    ),
    url(
        r'^resources/badge/new/$',
        BadgeCreateView.as_view(),
        name='badge_creation'
    ),
]
