from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import get_object_or_404

from braces.views import LoginRequiredMixin, UserFormKwargsMixin
# we use django-braces to have a generic login required mixin view and current user
# http://django-braces.readthedocs.io/en/latest

from apps.core.viewmixins import ModelNameViewMixin, ListViewMixin

from .forms import Model3dForm, BadgeForm
from .models import Model3d, Badge


# Model3d views
class Model3dDetailView(LoginRequiredMixin, ModelNameViewMixin, DetailView):
    """
    A class to implement the view to visualize a single model3d.
    """
    model = Model3d
    title = 'Consultation d\'un modèle 3D'

    def get_object(self, queryset=None):
        """
        Override get_object to increment model views for each call to
        DetailView
        """
        obj = super(Model3dDetailView, self).get_object(queryset)
        obj.views += 1
        obj.save()
        return obj


class Model3dListView(LoginRequiredMixin, ListViewMixin, ListView):
    """
    A class to implement the view to list 3d models.
    """
    model = Model3d
    title = 'Liste des modèles 3D'


class Model3dCreateView(LoginRequiredMixin, UserFormKwargsMixin, CreateView):
    """
    A class to implement a view to create 3d model.
    """
    model = Model3d
    title = 'Création d\'un modèle 3D'
    form_class = Model3dForm

    def get_success_url(self):
        return reverse("resources:model3d_list")

    def form_valid(self, form):
        """
        Override form_valid to give the Collector badge when current
        user has added more than 5 3d models.
        """
        self.object = form.save()
        user = self.object.user
        if not user.has_collector_badge and \
            Model3d.objects.filter(user=user).count() > 5:
            user.badges.add(get_object_or_404(Badge, title='Collector'))
            user.save()
            messages.info(
                self.request,
                'Félicitations, vous venez d\'obtenir le badge Collector !'
            )
        return super(Model3dCreateView, self).form_valid(form)


# Badge views
class BadgeDetailView(LoginRequiredMixin, ModelNameViewMixin, DetailView):
    """
    A class to implement the view to visualize a single badge.
    """
    model = Badge
    title = 'Consultation d\'un badge'


class BadgeListView(LoginRequiredMixin, ListView):
    """
    A class to implement the view to list badges.
    """
    model = Badge
    title = 'Liste des badges'


class BadgeCreateView(LoginRequiredMixin, CreateView):
    """
    A class to implement a view to create badge.
    """
    model = Badge
    title = 'Création d\'un badge'
    form_class = BadgeForm

    def get_success_url(self):
        return reverse("resources:badge_list")
