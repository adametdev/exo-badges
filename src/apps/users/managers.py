from django.contrib.auth.models import UserManager as UserManager_


class UserManager(UserManager_):
    """
    Specific manager for user models.
    """
    def get_queryset(self):
        """
        Override default queryset to filter and order base models.
        """
        return super(UserManager, self).get_queryset().filter(deleted=0).order_by('-id')
