import datetime

from django.shortcuts import get_object_or_404
from django.utils import timezone

from apps.resources.models import Badge


class UserRegistredMiddleware(object):
    """
    Specific widdleware to add the Pionneer badge when current user
    has joined the site since more than one year
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            date_one_year = timezone.now() - datetime.timedelta(days=365)
            if request.user.date_joined < date_one_year and \
                not request.user.badges.filter(title='Pionneer').exists():
                request.user.badges.add(get_object_or_404(Badge, title='Pionneer'))
                request.user.save()

        response = self.get_response(request)
        return response
