# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-18 16:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20161018_1143'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='has_collector_badge',
            field=models.BooleanField(default=False),
        ),
    ]
