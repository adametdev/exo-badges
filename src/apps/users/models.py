from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models

from apps.core.models import BaseModel

from .managers import UserManager


class User(BaseModel, AbstractUser):
    """
    Specific User model.
    """
    badges = models.ManyToManyField('resources.Badge', blank=True, related_name='users')
    # add has_collector_badge to avoid extra query to give collector badge to an user
    has_collector_badge = models.BooleanField(default=False)

    objects = UserManager()

    class Meta:
        verbose_name = 'Utilisateur'
        verbose_name_plural = 'Utilisateurs'
