from django.conf.urls import url, include

from rest_framework import routers

from .views import UserDetailView, UserListView, UserApiDetail


urlpatterns = [
    url(
        r'^users/(?P<pk>\d+)/$',
        UserDetailView.as_view(),
        name='user_detail'
    ),
    url(
        r'^users/$',
        UserListView.as_view(),
        name='user_list'
    ),
]

router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns += [
    url(r'^', include(router.urls)),
    url(r'^userbadgesapi/(?P<id>[0-9]+)/$', UserApiDetail.as_view()),
]
