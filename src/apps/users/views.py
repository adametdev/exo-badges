from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

from braces.views import LoginRequiredMixin

from apps.core.viewmixins import ModelNameViewMixin
from apps.resources.models import Badge
from apps.resources.serializers import BadgeSerializer

from .models import User


# Users views
class UserDetailView(LoginRequiredMixin, ModelNameViewMixin, DetailView):
    """
    A class to implement the view to visualize a single user.
    """
    model = User
    title = 'Consultation d\'un utilisateur'


class UserListView(LoginRequiredMixin, ListView):
    """
    A class to implement the view to list users.
    """
    model = User
    title = 'Liste des utilisateurs'


class UserApiDetail(APIView):
    """
    Retrieve a user instance.
    """
    def get_object(self, id):
        try:
            return User.objects.get(id=id)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        serializer = BadgeSerializer(Badge.objects.filter(users__pk=id), many=True)
        return Response(serializer.data)
