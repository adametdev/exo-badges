from .base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': 'localhost',
        'USER': get_env_variable('SKETCHFAB_DB_USER'),
        'NAME': get_env_variable('SKETCHFAB_DB_NAME'),
        'PASSWORD': get_env_variable('SKETCHFAB_DB_PASSWORD'),
    },
}

DEBUG = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEFAULT_FROM_EMAIL = 'webmaster-adam@gmail.com'

AUTH_PASSWORD_VALIDATORS = []

INSTALLED_APPS += [
    # External apps
    'django_extensions',
]
