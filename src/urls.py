from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views


# Administration urls
urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

# Apps urls
urlpatterns += [
    url(r'^', include('apps.core.urls', namespace='core')),
    url(r'^', include('apps.users.urls', namespace='users')),
    url(r'^', include('apps.resources.urls', namespace='resources')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
